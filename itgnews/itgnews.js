jQuery(function($) {

  // Temporary hack to make jQuery 1.5 work in disgusting IE9 Beta
  // http://bugs.jquery.com/ticket/8052#comment:14
  jQuery.support.noCloneEvent = !!window.addEventListener;

  // Cache $(window)
  var $window = $(window);

  var openedHeight = 160;
  var throbberMarginTop = openedHeight/2-24;

  var opened = false;
  var locked = false;
  var articleOpened = false;

  var itgnews = $('#itgnews');
  var header = itgnews.find('h2').after('<hr>');
  var hr = itgnews.find('hr').hide();
  var ogMaxWidth = itgnews.css('max-width');

  var wrapper = itgnews.wrap('<div id=itgnewswrapper></div>').parent().css({
    height: itgnews.outerHeight() + 10,
    maxWidth: ogMaxWidth
  });

  itgnews.css('position', 'absolute');

  var headerHeight = header.outerHeight(true);

  // Remember the size of the box when it's collapsed.
  // If the client is using ActiveX filter:...Shadow, it will add <strength>
  // pixels to each side, so we have to correct for that.
  var sizeAdjustment = 0;
  var filter = itgnews.css('filter');
  if (filter && filter != 'none' && filter != '') { 
    var strength = filter.match(/shadow\S+strength=(\d+)/i);
    if (strength)
      sizeAdjustment = strength[1] * 2;
  }
  var originalHeight = itgnews.height() - sizeAdjustment;

  // Navigation and close link
  var nav = $('<div id=itgnewsnav>').hide();

  var aPrev = $(' <a href=# title="Previous 5 articles">&laquo;</a> ').click(function(e) {
    e.preventDefault();
    loadHeadlines(-1);
  });

  var aFirst = $(' <a href=# title="Latest 5 articles">&bull;</a> ').click(function(e) {
    e.preventDefault();
    loadHeadlines(0);
  });

  var aNext = $(' <a href=# title="Next 5 articles">&raquo;</a> ').click(function(e) {
    e.preventDefault();
    loadHeadlines(1);
  });

  var aClose = $('<a href=# title="Close ITG News">x</a>').click(function(e) {
    e.stopPropagation();
    e.preventDefault();
    locked = false;
    itgnews.mouseleave();
  });

  nav.append(aPrev).append(aFirst).append(aNext).append(aClose);
  itgnews.append(nav);

  var reader = $('<div id=itgnewsreader>').hide();
  var throbber = $('<img src=itgnews/throbber.gif width=16 height=16 alt=Loading id=itgnewsthrobber>').css({marginTop: throbberMarginTop}).hide();

  itgnews.append(throbber).append(reader);

  var firstLoaded = false; // Only automatically load first set of headlines once
  function open() {
    if (opened)
      return;
    itgnews.stop(true).animate(
      { height: openedHeight },
      { complete: function() {
        detachHeaderClicks();
        if (!firstLoaded) {
          loadHeadlines(0);
          firstLoaded = true;
        }
      }
      }
    );
    nav.stop(true,true).delay(250).fadeInOrDont();
    reader.stop(true,true).delay(250).fadeInOrDont();
    hr.stop(true,true).delay(250).fadeInOrDont();
    opened = true;
  }

  function close() {
    if (locked) 
      return;
    nav.stop(true,true).fadeOutOrDont();
    reader.stop(true,true).fadeOutOrDont();
    hr.stop(true,true).fadeOutOrDont();
    itgnews.stop(true).delay(200).animate({ height: originalHeight });
    attachHeaderClicks();
    opened = false;
  }

  itgnews.hover(open, close);

  // Lock the box open when it's clicked so people can scroll, etc. if needed
  itgnews.click(function() {
    locked = true;
    itgnews.mouseenter();
  });

  // Attach the header click event so that clicking the widget when it's collapsed just opens it.
  var headerLinks = header.find('a');
  function attachHeaderClicks() {
    headerLinks.click(function(e) {
      e.preventDefault();
      open();
    });
  }
  attachHeaderClicks();

  // Detach the header click event so that when the widget is expanded, you can follow the links in the header
  function detachHeaderClicks() {
    headerLinks.unbind('click');
  }

  // Load some headlines. Pass -1 for previous 5, 0 for latest, and 1 for next 5.
  var index = 0, count = 5;
  function loadHeadlines(direction) {
    index = direction == 0 ? 0 : direction * count + index;
    reader.hide();
    reader.empty();
    articleOpened = false;
    throbber.show();
    $.getJSON( 'itgnews/itgnews.php?&json&i='+index+'&c='+count, function(json) {
      throbber.hide();
      reader.show();
      $.each(json, function(k,o) {

        var headline = $('<h3>' + o.title + '</h3>');
        reader.append(headline);

        // Add wingdings to urgent headlines
        if (typeof o.category == 'string')
          if (o.category.match(/urgent/))
            headline.text('» ' + headline.text());

        // Stash the click event so we can bind/unbind it
        headline.data('click', function() {

          if (articleOpened)
            return;

          var siblings = $(this).siblings();
          siblings.hide();

          var article = $('<div id=itgnewsarticle>');

          // Sanitize and format description
          var description = o.description.replace(/^\s*/, '<p>').replace(/\s*$/, '</p>').replace(/\n{1,}/g, '</p><p>');

          article.append($('<div class=itgnewsdescription>' + description + '</div>'));
          article.append($('<p><a href="' + o.link + '">' + o.link + '</a></p>'));
          reader.append(article);

          var back = $('<a href=#>&laquo; back</a>').click(function(e) {
            articleOpened = false;
            e.preventDefault();
            article.remove();
            siblings.fadeInOrDont();
            fitHeight();
          }); 
          article.append(back);

          article.hide().stop(true,true).fadeInOrDont('slow', fitHeight);
          articleOpened = true;

          // Rebind click so it closes the article
          headline.click(function(e) {
            e.stopPropagation();
            back.click();

            // Bind click back to stashed handler
            headline.unbind('click');
            headline.click(headline.data('click'));
          });

        });

        // Bind stashed click handler
        headline.click(headline.data('click'));

        // Stick the date in there. Make it pretty first. Use non-breaking spaces so it doesn't...break.
        var date = new Date(o.pubDate);
        var month = 'Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec'.split(' ')[date.getMonth()];
        var day = date.getDate();
        var year = date.getFullYear();
        var hours = date.getHours();
        var mod = hours < 12 ? 'am' : 'pm';
        hours = hours % 12;
        hours = hours == 0 ? 12 : hours;

        // Make sure minutes is 2 digits by prepending a zero then taking the last 2 digits
        var minutes = ('0' + date.getMinutes()).slice(-2);

        var time = hours + ':' + minutes + mod;
        var prettyDate = month + ' ' + day + ', ' + year + ' @ ' + time;

        headline.append($(' <span class="pubDate">  - ' + prettyDate + '</span>'));

      });

      reader.hide().stop(true,true).fadeInOrDont('slow', fitHeight);
    });
  }

  // Fit the height of itgnews to its contents. Call after changing contents of itgnews.
  function fitHeight() {
    var oldOpenedHeight = openedHeight;
    openedHeight = headerHeight + reader.outerHeight(true) + sizeAdjustment;
    if (oldOpenedHeight != openedHeight)
      itgnews.stop(true).animate({height: openedHeight});
  }

  // If client supports opacity, fadeIn. Otherwise show.
  $.fn.fadeInOrDont = function() {
    var method = $.support.opacity ? $.fn.fadeIn : $.fn.show;
    var args = arguments;

    // Don't let IE apply a speed
    if (args.length == 2 && method == $.fn.show)
      args = [0, args[1]];
    return this.each(function() {
      return method.apply($(this), args);
    });
  }

  // If client supports opacity, fadeOut. Otherwise hide.
  $.fn.fadeOutOrDont = function() {
    var method = $.support.opacity ? $.fn.fadeOut : $.fn.hide;
    var args = arguments;

    // Don't let IE apply a speed
    if (args.length == 2 && method == $.fn.show)
      args = [0, args[1]];
    return this.each(function() {
      return method.apply($(this), args);
    });
  }

  function fitWidth() {
    // Only fit height
    if (opened) {
      fitHeight();
    }
    // Set the max-width to the LEAST of the original width, the width of the
    // container, or the width of the window (with 40px fudge factor).
    itgnews.css({ maxWidth: Math.min(Math.min($window.width() - 40, parseInt(ogMaxWidth), parseInt(wrapper.parent().width()))) });
  }

  $window.resize(fitWidth);
  fitWidth();

  // Use Firebug or other console.log if available. Otherwise, just append
  // paragraphs to the body. Takes an array of objects, just like console.log
  function log() {
    try {
      console.log(arguments);
    }
    catch (e) {
      // Get copy of arguments as array
      var args = Array.prototype.slice.call(arguments);
      $('body').append('<pre>' + args.join());
    }
  }
});
