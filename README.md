itgnews is the RSS news reader for the ITG page of The Gevirtz School's web site. It displays the contents of the site's RSS feed in a slick widget.

http://education.ucsb.edu/itg

Copyright 2010-2011, UC Regents
Dual licensed under the MIT or GPL Version 2 licenses.


Installation Instructions
-------------------------

Copy the `itgnews` directory into the `/itg` directory on the web server.

In the /itg/index.html page, include jQuery and jQuery UI in the head of the ITG home page BEFORE including these tags in the head of the ITG home page:

    <link rel="stylesheet" type="text/css" href="itgnews/itgnews.css" />
    <!--[if lt IE 9]> <link rel="stylesheet" type="text/css" href="itgnews/itgnewsie.css" /> <![endif]-->
    <script src="itgnews/itgnews.js" type="text/javascript"></script>

Include these tags whererever you want to insert the reader:

    <div id="itgnews" class="shadowed"><h2><a href="itgnews/itgnews.php">ITG News</a> <a href="rss/rss.xml" title="RSS"><img src="itgnews/rss.png" alt="RSS" width="16" height="16" /></a></h2></div>



