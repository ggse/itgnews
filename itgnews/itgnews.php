<?php

/**
 * Get the arguments as URI parameters
 *
 * $index = which article we're starting at
 * $count = how many articles to retrieve
 * $chanl = the RSS channel to retrieve
 * $xmluri = the URI of the RSS XML feed
 */
$index = isset($_REQUEST['i']) ? $_REQUEST['i'] : 0;
$count = isset($_REQUEST['c']) ? $_REQUEST['c'] : 5;
$chanl = isset($_REQUEST['chanl']) ? $_REQUEST['chanl'] : 'ITG News RSS Feed';
$xmluri = isset($_REQUEST['xml']) ? $_REQUEST['xml'] : '../rss/rss.xml';
$json = isset($_REQUEST['json']);

if ($json)
  header('Content-Type: application/json');
else
  header('Content-Type: text/html');

// Prevent caching
header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Pragma: no-cache');

if (!$xml = simplexml_load_file($xmluri)) die("Couldn't open $xmluri");

/*
 * Handle for the RSS channel that we're dealing with. This script only 
 * supports one channel at a time. If there are multiple channels of the same 
 * name, the last one encountered is used.
 */
$channel = null;

foreach($xml->channel as $c)
  if ($c->title == $chanl) $channel = $c;

if ($channel) {

  // Construct the JSON object from the PHP XML object
  $items = array();

  foreach($channel->item as $i)
    $items[] = $i;

  $size = sizeof($items);

  // Allow index values to wrap
  $index = $index % $size;

  // Negative indexes count backwards from the end
  if ($index < 0)
    $index = $size + $index;

  // Jagged indexes need to be rounded to nearest $count multiple
  if (($mod = $index % $count) != 0)
    $index = $index - $mod + $count;

  // But if the index ends up higher than the size, we need to wrap it to zero again
  if ($index > $size && $json)
    $index = 0;

  if ($json) {
    $slice = array_slice($items, $index, $count);
    print json_encode($slice);
  }
  else {

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>ITG News</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
      * {
        margin:0;
        padding:0;
      }
      a img {
        border:0;
      }
      body {
        font-family:sans-serif;
        font-size:12px;
      }
      .date {
      }
      .article {
        border-bottom:1px solid #999;
        margin:10px;
        padding:10px;
      }
      #content {
        margin:auto;
        width:500px;
      }
      h2 {
        font-size: 16px;
      }
      .date {
        font-size:10px;
      }
      .description {
        padding:10px;
      }
      .description p {
        margin:10px;
      }
    </style>
  </head>
  <body>
    <div id="content">
      <h1><a href="/itg">ITG</a> News <a href="<?php echo $xmluri ?>"><img src="rss.png" height="16" width="16" alt="RSS" /></a></h1>
      <?php
      foreach ($items as $item) {
      ?>
      <div class="article">
        <h2><?php echo $item->title ?></h2>
        <p class="date"><?php echo $item->pubDate ?></p>
        <div class="description"><?php

        $description = $item->description;
        $description = preg_replace('/^\s*$/', '<br />', $description);
        $description = preg_replace('/\n{1,}/', '<br /><br />', $description);

        print $description;

        ?></div>

        <p>Link: <a href="<?php echo $item->link ?>"><?php echo $item->link ?></a></p>
      </div>
      <?php
      }

      ?>
    </div>
  </body>
</html>
<?php

  }
}

ob_flush();

